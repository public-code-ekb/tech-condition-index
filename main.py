# Диапазоны индекса технического состояния
ranges_tech_condition = [
    {
        "id": 1,
        "min_index": 0,
        "max_index": 25,
        "type_key": "critical",
        "type_title": "Критическое",
        "color_signal_key": "red",
        "color_signal_title": "Красный",
        "type_technical_impact": "Вывод из эксплуатации, техническое перевооружение и реконструкция",
    },
    {
        "id": 2,
        "min_index": 25,
        "max_index": 50,
        "type_key": "unsatisfactory",
        "type_title": "Неудовлетворительное",
        "color_signal_key": "orange",
        "color_signal_title": "Оранжевый",
        "type_technical_impact": "Дополнительное техническое обслуживание и ремонт, усиленный контроль технического состояния, техническое перевооружение",
    },
    {
        "id": 3,
        "min_index": 50,
        "max_index": 70,
        "type_key": "satisfactory",
        "type_title": "Удовлетворительное",
        "color_signal_key": "yellow",
        "color_signal_title": "Желтый",
        "type_technical_impact": "Усиленный контроль технического состояния, капитальный ремонт, реконструкция",
    },
    {
        "id": 4,
        "min_index": 70,
        "max_index": 85,
        "type_key": "good",
        "type_title": "Хорошее",
        "color_signal_key": "green",
        "color_signal_title": "Зеленый",
        "type_technical_impact": "По результатам планового диагностирования",
    },
    {
        "id": 5,
        "min_index": 85,
        "max_index": 100,
        "type_key": "very_good",
        "type_title": "Очень хорошее",
        "color_signal_key": None,
        "color_signal_title": "",
        "type_technical_impact": "Плановое диагностирование",
    },
]

# Группы оборудования
equipment_group = [
    {
        "key": "electrical_equipment",
        "value": "Электротехническое оборудование",
    },
]

# Классы оборудования
equipment_class = [
    {
        "key": "power_transformer",
        "value": "Трансформатор (автотрансформатор) силовой",
        "equipment_group": "electrical_equipment",
    },
]

# Функциональные узлы
functional_node = [
    {
        "key": "vv",
        "value": "Высоковольтный ввод",
        "equipment_class": "power_transformer",
    },
    {
        "key": "vo",
        "value": "Вспомогательное оборудование",
        "equipment_class": "power_transformer",
    },
    {
        "key": "is",
        "value": "Изоляционная система",
        "equipment_class": "power_transformer",
    },
    {
        "key": "mp",
        "value": "Магнитопровод",
        "equipment_class": "power_transformer",
    },
    {
        "key": "ot",
        "value": "Обмотки трансформатора",
        "equipment_class": "power_transformer",
    },
    {
        "key": "rn",
        "value": "Система регулирования напряжения",
        "equipment_class": "power_transformer",
    },
]

# Группы параметров функционального узла
param_group = [
    {
        "key": "",
        "value": "",
        "functional_node": "",
    },
    {
        "key": "",
        "value": "",
        "functional_node": "",
    },
    {
        "key": "",
        "value": "",
        "functional_node": "",
    },
    {
        "key": "",
        "value": "",
        "functional_node": "",
    },
    {
        "key": "",
        "value": "",
        "functional_node": "",
    },
    {
        "key": "",
        "value": "",
        "functional_node": "",
    },
    {
        "key": "",
        "value": "",
        "functional_node": "",
    },
    {
        "key": "",
        "value": "",
        "functional_node": "",
    },
    {
        "key": "",
        "value": "",
        "functional_node": "",
    },
    {
        "key": "",
        "value": "",
        "functional_node": "",
    },
]

# Параметры функционального узла
param_node = []
